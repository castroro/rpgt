/*
 *
 * Author: Alejandro Castro Royo <castroro@esat-alumni.com>
 *
 */


#ifndef __MY_INPUT_H__
#define __MY_INPUT_H__ 1

#include "ESAT/input.h"

class Input {

    public:
        Input();
        ~Input() {};
        //
        // @brief      { put all the input variables to false }
        //
        void resetInput();
        /**
         * @brief      { controls whats keys are pressed and activate it }
         */
        void update();
        //------------------ PLAYER MOVEMENT
        bool arrow_up_;
        bool arrow_down_;
        bool arrow_right_;
        bool arrow_left_;
        //------------------- END PLAYER MOVEMENT

        bool action_space_bar_;
        bool confirm_action_;
        bool no_confirm_action_;
        bool status_button_S_;
        bool exit_game_esc_;

        //--------------------Camera Zoom
        bool add_zoom_;
        bool substract_zoom_;

    private:



};

#endif