/*
 *
 * Author: Alejandro Castro Royo <castroro@esat-alumni.com>
 *
 */


#ifndef _MY_GRID_
#define _MY_GRID_ 1

class Grid {

    public:

        Grid();
        ~Grid();

        /**
           * @brief grid user constructor.
           * @param number of rows and cols
           */
        void init(int rows,  int cols);
        /**
        * @brief put a value in the concret tiled.
        * @param
        */
        void initWithValue(int rows, int cols, int value);
        /**
        * @brief objects of enemy characters.
        *@param
        */
        void zeros();
        /**
        * @brief objects of enemy characters.
        *@param
        */
        int getElement(int row, int col);

        int getElement2(int data);
        /**
        * @brief objects of enemy characters.
        *@param
        */
        void setElement(int row, int col, int value);
        /**
        * @brief objects of enemy characters.
        * @param
        */
        void print();
        /**
        * @brief objects of enemy characters.
        * @param
        */
        Grid add(Grid *auxGrid);
        /**
        * @brief objects of enemy characters.
        * @param
        */
        Grid substract(Grid *auxGrid);






        /**
           * @brief objects of enemy characters.
           * @param
           */
        int checkInt(int number);
        int *data_; //los datos con direccion,
        //suelen ponerse privados para evitar marrones
        int rows_;
        int cols_;

    public:


};

#endif