
#include "init_scene.h"

InitScene::InitScene() {

  init_completed_ = false;
  my_input_.resetInput();


}


InitScene::~InitScene() {


}

void InitScene::init() {

  intro_text_.init("../fonts/endor.ttf", 50, 0, 0, 255, 0, 255);
  my_school_logo_.spriteInit("../posibles/arte/init/esat.png");
  my_name_awesome_logo_.spriteInit("../posibles/arte/init/name.png");
  init_completed_ = true;
}



void InitScene::theInput() {
  my_input_.update();
}

void InitScene::update() {

  if (my_input_.action_space_bar_) {

    GameManager::instance().current_scene_ = GameManager::kScene_StartScene_Team;

  }



}

void InitScene::draw() {

  intro_text_.drawText(250, 450, "Welcome, press space to continue");

  my_name_awesome_logo_.drawSprite(50, 170, 0, 1, 1, 0, 0);
  my_school_logo_.drawSprite(10, 10, 0, 0.3, 0.3, 0, 0);


}
