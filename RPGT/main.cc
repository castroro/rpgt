
#include <stdio.h>
#include <stdlib.h>


#include "scene_selector.h"
#include "mysprite.h"

int ESAT::main(int argc, char **argv) {
  // Init game
  srand(time(NULL));


  SceneSelector my_scene_selector;


  ESAT::WindowInit(800, 580);
  ESAT::DrawSetStrokeColor(255, 255, 255, 255);

  //ESAT::DrawSetTextFont("greek.ttf");
  ESAT::DrawSetTextSize(20);
  // my_sprite.spriteInit("../Posibles/Arte/intento.png");

  my_scene_selector.init();



  // my_scene_selector.init();



  while (ESAT::WindowIsOpened() && !ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape)) {

    my_scene_selector.input();
    my_scene_selector.update();

    ESAT::DrawClear(0, 0, 0);

    ESAT::DrawBegin();


    my_scene_selector.draw();




    //ESAT::DrawSetStrokeColor(255, 255, 255, 255);
    ESAT::DrawEnd();
    ESAT::WindowFrame();
  }




  ESAT::WindowDestroy();
  return 0;
}