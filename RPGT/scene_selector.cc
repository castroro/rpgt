
#include "scene_selector.h"

#include "game_manager.h"

SceneSelector::SceneSelector() {

  end_of_game_ = false;

}



void SceneSelector::init() {
  intro_scene_.init();
  choose_team_scene_.init();
  game_scene_.init();
  combat_scene_.init();

  //GameManager::instance().soloud.play(GameManager::instance().myWav);


}

/*
  enum enum_of_Scene {
  kScene_IntroScene = 0,
  kScene_StartScene_Team,
  kScene_GameScene,
  kScene_CombatScene,
  kScene_ShopScene,
  kScene_GameOverScene,

};
*/
void SceneSelector::input() {


  switch (GameManager::instance().current_scene_) {

    case GameManager::kScene_IntroScene:


      intro_scene_.theInput();
      break;

    case GameManager::kScene_StartScene_Team:


      choose_team_scene_.input();
      break;

    case GameManager::kScene_GameScene:

      game_scene_.input();
      break;

    case GameManager::kScene_CombatScene:

      combat_scene_.input();

      break;



  }

}

void SceneSelector::update() {

  switch (GameManager::instance().current_scene_) {

    case GameManager::kScene_IntroScene:

      intro_scene_.update();

      break;

    case GameManager::kScene_StartScene_Team:

      choose_team_scene_.update();

      break;

    case GameManager::kScene_GameScene:


      game_scene_.update();

      break;

    case GameManager::kScene_CombatScene:

      combat_scene_.update();

      break;


  }


}

void SceneSelector::draw() {

  switch (GameManager::instance().current_scene_) {

    case GameManager::kScene_IntroScene:

      intro_scene_.draw();


      break;

    case GameManager::kScene_StartScene_Team:

      choose_team_scene_.draw();

      break;
    case GameManager::kScene_GameScene:

      game_scene_.draw();
      // GameManager::instance().my_walking_grid_.print();

      break;

    case GameManager::kScene_CombatScene:

      combat_scene_.draw();

      break;
  }

}