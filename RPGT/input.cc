#include "input.h"

Input::Input() {

  arrow_up_ = false;
  arrow_down_ = false;
  arrow_right_ = false;
  arrow_left_ = false;

  action_space_bar_ = false;
  status_button_S_ = false;
  exit_game_esc_ = false;
  add_zoom_ = false;
  substract_zoom_ = false;

}

void Input::resetInput() {

  arrow_up_ = false;
  arrow_down_ = false;
  arrow_right_ = false;
  arrow_left_ = false;

  action_space_bar_ = false;
  status_button_S_ = false;
  exit_game_esc_ = false;

  add_zoom_ = false;

  substract_zoom_ = false;
}

void Input::update() {

  resetInput();

  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Up)) {
    arrow_up_ = true;
  }
  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Down)) {
    arrow_down_ = true;
  }
  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Right)) {
    arrow_right_ = true;
  }
  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Left)) {
    arrow_left_ = true;
  }


  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape)) {
    exit_game_esc_ = true;
  }

  if (ESAT::IsKeyPressed('S')) {

    status_button_S_ = true;

  }

  if (ESAT::IsKeyDown('Z')) {

    add_zoom_ = true;

  }

  if (ESAT::IsKeyDown('X')) {

    substract_zoom_ = true;

  }

  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Space)) {
    action_space_bar_ = true;
  }

}