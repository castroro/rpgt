#ifndef __MY_CHARACTER_H__
#define __MY_CHARACTER_H__ 1





class Character {

    public:
        Character();
        ~Character();

        enum CharacterType {

            kCharacter_Warrior = 1,
            kCharacter_Roge,
            kCharacter_Mage,
            kChatacter_Healer,
            kCharacter_SwordMan,
            kCharacter_Enemy
        };

        enum EnemyType {
            kEnemy_easy = 1,
            kEnemy_medium,
            kEnemy_hard,
            kEnemy_boss
        };


        /**
         * @brief Initialize the character player
         * @param enum of the type of class, and the id in game
         */

        void chooseType(CharacterType type, short int own_id, char name[32]);

        /**
          * @brief complement of chooseType function, for the characters with magic skills
          *@param  enum of the type of playable class
          */
        void magicPowers(CharacterType type);

        /**
          * @brief function that serves to upgrade the level of the character
          *@param  enum of the type of playable class
          */
        void levelUp(short int type);
        /**
            * @brief inilzialice the enemies of the game
            *@param num of the type of class, and the id in game
            */

        void createTypeEnemy(EnemyType type, short int own_id);

        /**
         * @brief reset the number of movements when the turn is over
         *
         */
        void resetMovement();


        bool alive_;
        bool shopping_;
        bool character_has_magic_;
        bool is_moving_;
        bool is_attacking_;
        bool battle_posibility_;

        short int heal_points_;
        short int max_heal_points_;
        short int magic_points_;
        short int max_magic_points_;
        short int attack_power_;
        short int defense_power_;
        short int magic_attack_power_;
        short int magic_defense_power_;
        short int dodge_power_;


        short int current_level_;
        short int experience_to_upgrade_level;
        short int experience_;
        short int *items_;

        short int movements_;
        short int movements_reset_;
        short int enum_type_;
        short int own_id_;

        float drawable_pos_x_;
        float drawable_pos_y_;
        int array_position_;
        int array_id_;
        char name_[32];

        short int gold_;



    private:

};


#endif
