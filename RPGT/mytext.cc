#include "mytext.h"

/*
 void DrawSetTextFont(const char *name);
  void DrawSetTextSize(float size);
  void DrawSetTextBlur(float blur_radius);
  void DrawText(float x, float y, const char *text);
*/

void Text::init(const char *name, float size, float blur_radius,
                unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha) {


  ESAT::DrawSetFillColor(red, green, blue, alpha);
  ESAT::DrawSetTextFont(name);
  ESAT::DrawSetTextSize(size);
  ESAT::DrawSetTextBlur(blur_radius);

}

void Text::drawText(float x, float y, const char *text) {

  position_x_ = x;
  position_y_ = y;
  text_ = text;

  ESAT::DrawText(position_x_, position_y_, text);


}