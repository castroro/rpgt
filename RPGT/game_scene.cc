
#include "game_scene.h"

//typedef GameManager GM; NO

GameScene::GameScene() {

  enter_battle_ = false;
  enter_shop_ = false;
  init_completed_ = false;
  movement_direction_ = 0;
  my_input_.resetInput();
  number_of_grid_rows_ = 18;
  number_of_grid_cols_ = 25;
  GameManager::instance().my_characters_[0].is_moving_ = true;
  GameManager::instance().player_turn_counter_ = 0;
  draw_player_stats_ = false;
  its_an_enemy_ = false;
  zoom_ = 0.1;
  characters_zoom_ = 0;

}

GameScene::~GameScene() {


}

void GameScene::init() {

  initGrid();
  initWalkingGrid();
  initPlayersPositions();

  map_sprite_.spriteInit("../posibles/arte/map.png");
  characters_sprites_[0].spriteInit("tupac.png");
  characters_sprites_[1].spriteInit("../posibles/arte/player/map_player/healer/sprite_1.png");
  characters_sprites_[2].spriteInit("../posibles/arte/player/map_player/wizard/sprite_1.png");
  characters_sprites_[3].spriteInit("../posibles/arte/player/map_player/roge/sprite_1.png");


  //////////////////////////////////////////////////////////////////////////////
  enemies_sprites_[0].spriteInit("../posibles/arte/enemies/m_rig.png");
  enemies_sprites_[1].spriteInit("../posibles/arte/enemies/m_rig.png");
  enemies_sprites_[2].spriteInit("../posibles/arte/enemies/m_rig.png");

  enemies_sprites_[3].spriteInit("../posibles/arte/enemies/m_down.png");
  enemies_sprites_[4].spriteInit("../posibles/arte/enemies/m_down.png");
  enemies_sprites_[5].spriteInit("../posibles/arte/enemies/m_down.png");
  //EASY MONSTERS
  enemies_sprites_[6].spriteInit("../posibles/arte/enemies/m_left.png");
  enemies_sprites_[7].spriteInit("../posibles/arte/enemies/m_left.png");
  enemies_sprites_[8].spriteInit("../posibles/arte/enemies/m_left.png");

  enemies_sprites_[9].spriteInit("../posibles/arte/enemies/m_up.png");
  enemies_sprites_[10].spriteInit("../posibles/arte/enemies/m_up.png");
  enemies_sprites_[11].spriteInit("../posibles/arte/enemies/m_up.png");
  //////////////////////////////////////////////////////////////////////////////
  enemies_sprites_[12].spriteInit("../posibles/arte/enemies/orc_right.png");

  enemies_sprites_[13].spriteInit("../posibles/arte/enemies/orc_down.png");
  //MEDIUM MONSTERS
  enemies_sprites_[14].spriteInit("../posibles/arte/enemies/orc_left.png");

  enemies_sprites_[15].spriteInit("../posibles/arte/enemies/orc_up.png");
  //////////////////////////////////////////////////////////////////////////////
  enemies_sprites_[16].spriteInit("../posibles/arte/enemies/orc_right.png");

  enemies_sprites_[17].spriteInit("../posibles/arte/enemies/orc_down.png");
  //HARD MONSTERS
  enemies_sprites_[18].spriteInit("../posibles/arte/enemies/orc_left.png");

  enemies_sprites_[19].spriteInit("../posibles/arte/enemies/orc_up.png");
  //////////////////////////////////////////////////////////////////////////////



  movements_on_screen_number_.init("../fonts/endor.ttf", 50, 0, 0, 0, 0, 255);
  movements_on_screen_text_.init("../fonts/endor.ttf", 50, 0, 0, 0, 0, 255);

  for (int i = 0; i < 8; ++i) {
    character_stats_numbers_[i].init("../fonts/endor.ttf", 50, 0, 0, 0, 0, 255);
    character_stats_texts_[i].init("../fonts/endor.ttf", 40, 0, 0, 0, 0, 255);
  }

  for (short int i = 0; i < GameManager::instance().enemy_counter_; ++i) {

    if (i < 12) {
      GameManager::instance().enemies_[i].createTypeEnemy(GameManager::instance()
          .enemies_[i].kEnemy_easy, i + 5);

    }
    if (i >= 12 && i < 15) {

      GameManager::instance().enemies_[i].createTypeEnemy(GameManager::instance()
          .enemies_[i].kEnemy_medium, i + 5);

    }
    if (i >= 15 && i < 18) {

      GameManager::instance().enemies_[i].createTypeEnemy(GameManager::instance()
          .enemies_[i].kEnemy_hard, i + 5);

    }
    if (i >= 18) {

      GameManager::instance().enemies_[i].createTypeEnemy(GameManager::instance()
          .enemies_[i].kEnemy_boss, i + 5);

    }
  }

  initEnemiesPositions();
  init_completed_ = true;
}


void GameScene::input() {
  my_input_.update();

}

void GameScene::update() {

  movementLogic();

  if (my_input_.arrow_up_) {
    movement_direction_ = 1;
    checkBattle();
    if (checkMove()) {
      movePlayerOnGrid();
      drawPlayerOnMap();
    }
  }
  //-----------------------------------------
  if (my_input_.arrow_down_) {
    movement_direction_ = 2;
    checkBattle();
    if (checkMove()) {
      movePlayerOnGrid();
      drawPlayerOnMap();
    }

  }

  if (my_input_.arrow_right_) {
    movement_direction_ = 3;
    checkBattle();
    if (checkMove()) {
      movePlayerOnGrid();
      drawPlayerOnMap();
    }
  }

  if (my_input_.arrow_left_) {
    movement_direction_ = 4;
    checkBattle();
    if (checkMove()) {
      movePlayerOnGrid();
      drawPlayerOnMap();
    }

  }

  if (my_input_.add_zoom_) {


    GameManager::instance().my_walking_grid_.print();

  }

  if (my_input_.substract_zoom_) {

    characters_zoom_ = zoom_;

    applyZoom((-1));

  }
  //-----------------------------------------
  if (my_input_.action_space_bar_ && GameManager::instance().my_characters_
      [GameManager::instance().player_turn_counter_].battle_posibility_) {

    GameManager::instance().my_characters_[GameManager::instance()
                                           .player_turn_counter_].movements_ = 0;

    GameManager::instance().my_characters_[GameManager::instance()
                                           .player_turn_counter_].is_attacking_  = true;

    GameManager::instance().current_scene_ = GameManager::kScene_CombatScene;



  }

  if (my_input_.status_button_S_) {
    draw_player_stats_ = true;

  }
  else draw_player_stats_ = false;

}

void GameScene::movementLogic() {

  Character *players = GameManager::instance().my_characters_;
  short int& player_turn = GameManager::instance().player_turn_counter_;


  if (players[player_turn].movements_ <= 0) {

    players[player_turn].is_moving_ = false;
    players[player_turn].resetMovement();

    player_turn += 1;

    if (player_turn > 3) {
      player_turn = 0;
    }
    players[player_turn].is_moving_ = true;

  }


}


void GameScene::movePlayerOnGrid() {

  GameManager& G = GameManager::instance();
  Character *players = GameManager::instance().my_characters_;
  Grid& grid = GameManager::instance().my_walking_grid_;
  short int& player_turn = GameManager::instance().player_turn_counter_;



  switch (movement_direction_) {

    case 1:
      //UP

      //check, and current position to 0;
      grid.data_[players[player_turn].array_position_] = 0;
      //movement of the player
      players[player_turn].array_position_ -= number_of_grid_cols_;
      //save in the new position the value of the character id;
      grid.data_[players[player_turn].array_position_] =
        players[player_turn].own_id_;

      players[player_turn].movements_--;
      //  }

      break;

    case 2:
      //DOWN


      grid.data_[players[player_turn].array_position_] = 0;

      players[player_turn].array_position_ += number_of_grid_cols_;

      grid.data_[players[player_turn].array_position_] =
        players[player_turn].own_id_;

      players[player_turn].movements_--;



      break;

    case 3:
      //RIGHT

      grid.data_[players[player_turn].array_position_] = 0;

      players[player_turn].array_position_ += 1;


      grid.data_[players[player_turn].array_position_] =
        players[player_turn].own_id_;

      players[player_turn].movements_--;

      break;

    case 4:
      //LEFT

      grid.data_[players[player_turn].array_position_] = 0;

      players[player_turn].array_position_ -= 1;


      grid.data_[players[player_turn].array_position_] =
        players[player_turn].own_id_;

      players[player_turn].movements_--;

      break;

  }

}

void GameScene::drawcheckBattle() {

  Character *players = GameManager::instance().my_characters_;
  short int& player_turn = GameManager::instance().player_turn_counter_;


  float points[10];


  switch (GameManager::instance().battle_indicator_) {

    case 1:

      points[0] = players[player_turn].drawable_pos_x_;
      points[1] = players[player_turn].drawable_pos_y_;

      points[2] = players[player_turn].drawable_pos_x_ + 36;
      points[3] = players[player_turn].drawable_pos_y_;

      points[4] =  players[player_turn].drawable_pos_x_ + 36;
      points[5] =  players[player_turn].drawable_pos_y_ - 32;

      points[6] = players[player_turn].drawable_pos_x_ ;
      points[7] = players[player_turn].drawable_pos_y_ - 32;

      points[8] =  players[player_turn].drawable_pos_x_;
      points[9] =  players[player_turn].drawable_pos_y_;

      ESAT::DrawSetStrokeColor(0, 0, 0, 255);
      if (its_an_enemy_) ESAT::DrawSetFillColor(200, 0, 0, 100);
      else ESAT::DrawSetFillColor(0, 200, 0, 100);
      ESAT::DrawSolidPath(points, 5, true);


      break;

    case 2:

      points[0] = players[player_turn].drawable_pos_x_;
      points[1] = players[player_turn].drawable_pos_y_ + 36;

      points[2] = players[player_turn].drawable_pos_x_ + 36;
      points[3] = players[player_turn].drawable_pos_y_ + 36;

      points[4] =  players[player_turn].drawable_pos_x_ + 36;
      points[5] =  players[player_turn].drawable_pos_y_ + 72;

      points[6] = players[player_turn].drawable_pos_x_ ;
      points[7] = players[player_turn].drawable_pos_y_ + 72;

      points[8] =  players[player_turn].drawable_pos_x_;
      points[9] =  players[player_turn].drawable_pos_y_ + 36;


      ESAT::DrawSetStrokeColor(0, 0, 0, 255);
      if (its_an_enemy_) ESAT::DrawSetFillColor(200, 0, 0, 100);
      else ESAT::DrawSetFillColor(0, 200, 0, 100);
      ESAT::DrawSolidPath(points, 5, true);


      break;

    case 3:

      points[0] = players[player_turn].drawable_pos_x_ + 36;
      points[1] = players[player_turn].drawable_pos_y_ + 4;

      points[2] = players[player_turn].drawable_pos_x_ + 72;
      points[3] = players[player_turn].drawable_pos_y_ + 4;

      points[4] =  players[player_turn].drawable_pos_x_ + 72;
      points[5] =  players[player_turn].drawable_pos_y_ + 36;

      points[6] = players[player_turn].drawable_pos_x_  + 36;
      points[7] = players[player_turn].drawable_pos_y_  + 36;

      points[8] =  players[player_turn].drawable_pos_x_ + 36;
      points[9] =  players[player_turn].drawable_pos_y_ + 4;

      ESAT::DrawSetStrokeColor(0, 0, 0, 255);
      if (its_an_enemy_) ESAT::DrawSetFillColor(200, 0, 0, 100);
      else ESAT::DrawSetFillColor(0, 200, 0, 100);
      ESAT::DrawSolidPath(points, 5, true);

      break;


    case 4:

      points[0] = players[player_turn].drawable_pos_x_;
      points[1] = players[player_turn].drawable_pos_y_ + 4;

      points[2] = players[player_turn].drawable_pos_x_ - 36;
      points[3] = players[player_turn].drawable_pos_y_ + 4;

      points[4] =  players[player_turn].drawable_pos_x_ - 36;
      points[5] =  players[player_turn].drawable_pos_y_ + 36;

      points[6] = players[player_turn].drawable_pos_x_;
      points[7] = players[player_turn].drawable_pos_y_  + 36;

      points[8] =  players[player_turn].drawable_pos_x_;
      points[9] =  players[player_turn].drawable_pos_y_ + 4;

      ESAT::DrawSetStrokeColor(0, 0, 0, 255);
      if (its_an_enemy_) ESAT::DrawSetFillColor(200, 0, 0, 100);
      else ESAT::DrawSetFillColor(0, 200, 0, 100);
      ESAT::DrawSolidPath(points, 5, true);

      break;

  }
}



void GameScene::checkBattle() {

  GameManager& G = GameManager::instance();
  Character *players = GameManager::instance().my_characters_;
  Grid& grid = GameManager::instance().my_walking_grid_;
  short int& player_turn = GameManager::instance().player_turn_counter_;



  switch (movement_direction_) {

    case 1:

      if (grid.data_[players[player_turn].array_position_
                     - (number_of_grid_cols_)] != 194 &&
          grid.data_[players[player_turn].array_position_
                     - (number_of_grid_cols_)] != 0) {

        GameManager::instance().battle_indicator_ = 1;

        G.enemy_id_ = grid.getElement2(players[player_turn].array_position_
                                       - (number_of_grid_cols_)) - 5;

      }

      else {
        GameManager::instance().battle_indicator_ = 0;
        players[player_turn].battle_posibility_ = false;
      }

      break;

    case 2:
      //DOWN

      if (grid.data_[players[player_turn].array_position_
                     + (number_of_grid_cols_)] != 194 &&
          grid.data_[players[player_turn].array_position_
                     + (number_of_grid_cols_ )] != 0) {

        GameManager::instance().battle_indicator_ = 2;
        G.enemy_id_ = grid.getElement2(players[player_turn].array_position_
                                       + (number_of_grid_cols_)) - 5;

      }
      else {
        GameManager::instance().battle_indicator_ = 0;
        players[player_turn].battle_posibility_ = false;
      }

      break;

    case 3:
      //RIGHT

      if (grid.data_[players[player_turn].array_position_
                     + (1)] != 194 &&
          grid.data_[players[player_turn].array_position_
                     + (1)] != 0 ) {

        GameManager::instance().battle_indicator_ = 3;
        G.enemy_id_ = grid.getElement2(players[player_turn].array_position_
                                       + (1)) - 5;


      }
      else {
        GameManager::instance().battle_indicator_ = 0;
        players[player_turn].battle_posibility_ = false;
      }

      break;

    case 4:
      //LEFT

      if (grid.data_[players[player_turn].array_position_
                     - (1)] != 194 &&
          grid.data_[players[player_turn].array_position_
                     - (1)] != 0 ) {

        GameManager::instance().battle_indicator_ = 4;
        G.enemy_id_ = grid.getElement2(players[player_turn].array_position_
                                       - (1)) - 5;

      }

      else {
        GameManager::instance().battle_indicator_ = 0;
        players[player_turn].battle_posibility_ = false;
      }

      break;

  }


}


bool  GameScene::checkMove() {

  GameManager& G = GameManager::instance();
  Character *players = GameManager::instance().my_characters_;
  Grid& grid = GameManager::instance().my_walking_grid_;
  short int& player_turn = GameManager::instance().player_turn_counter_;

  if (players[player_turn].movements_ <= 0) {

    players[player_turn].is_moving_ = false;
    players[player_turn].resetMovement();

    player_turn += 1;

    if (player_turn > 3) {
      player_turn = 0;
    }
    players[player_turn].is_moving_ = true;

  }

  if (players[player_turn].is_moving_ == true) {

    switch (movement_direction_) {

      case 1:
        //UP
        if (grid.data_[players[player_turn].array_position_
                       - number_of_grid_cols_] != 0 ) {


          if (grid.data_[players[player_turn].array_position_
                         - (number_of_grid_cols_)] > 4
              &&
              grid.data_[players[player_turn].array_position_
                         - (number_of_grid_cols_)] < 30) {

            its_an_enemy_ = true;
            players[player_turn].battle_posibility_ = true;
            //G.enemy_id_ = grid.getElement2(players[player_turn].array_position_
            //                                - (number_of_grid_cols_)) - 5 ;

          }

          else {
            its_an_enemy_ = false;
            players[player_turn].battle_posibility_ = false;
          }
          return false;
        }

        else {
          return true;
        }
        break;

      case 2:
        //DOWN
        if (grid.data_[players[player_turn].array_position_
                       + number_of_grid_cols_] != 0) {

          if (grid.data_[players[player_turn].array_position_
                         + (number_of_grid_cols_)] > 4 &&
              grid.data_[players[player_turn].array_position_
                         + (number_of_grid_cols_)] < 30) {
            its_an_enemy_ = true;
            players[player_turn].battle_posibility_ = true;
            // G.enemy_id_ = grid.getElement2(players[player_turn].array_position_
            //                               + (number_of_grid_cols_)) - 5 ;
          }

          else {
            its_an_enemy_ = false;
            players[player_turn].battle_posibility_ = false;
          }
          return false;
        }

        else {
          return true;
        }

        break;

      case 3:
        //RIGHT
        if (grid.data_[players[player_turn].array_position_ + 1] != 0) {

          if (grid.data_[players[player_turn].array_position_
                         + (1)] > 4 &&
              grid.data_[players[player_turn].array_position_
                         + (1)] < 30) {
            its_an_enemy_ = true;
            players[player_turn].battle_posibility_ = true;

            // G.enemy_id_ = grid.getElement2(players[player_turn].array_position_
            //                                + (1) ) - 5;
          }

          else {
            its_an_enemy_ = false;
            players[player_turn].battle_posibility_ = false;
          }
          return false;
        }

        else {
          return true;
        }

        break;

      case 4:
        //LEFT
        if (grid.data_[players[player_turn].array_position_ - 1] != 0) {

          if (grid.data_[players[player_turn].array_position_
                         - (1)] > 4                     &&
              grid.data_[players[player_turn].array_position_
                         - (1)] < 30) {
            its_an_enemy_ = true;
            players[player_turn].battle_posibility_ = true;
            // G.enemy_id_ = grid.getElement2(players[player_turn].array_position_
            //                                - (1) ) - 5;
          }

          else {
            its_an_enemy_ = false;
            players[player_turn].battle_posibility_ = false;
          }
          return false;
        }

        else {
          return true;
        }

        break;

    }

  }
  return false;

}


void GameScene::draw() {

  GameManager& G = GameManager::instance();

  Character *enemies = GameManager::instance().enemies_;

  map_sprite_.drawSprite(0, 0, 0,  1, 1 , 0, 0);

  drawPlayerTurnIndicator();
  characters_sprites_[0].drawSprite( G.my_characters_[0].drawable_pos_x_,
                                     G.my_characters_[0].drawable_pos_y_,
                                     0, 0.5, 0.5, 0, 0);

  characters_sprites_[1].drawSprite( G.my_characters_[1].drawable_pos_x_,
                                     G.my_characters_[1].drawable_pos_y_,
                                     0, 0.5, 0.5, 0, 0);

  characters_sprites_[2].drawSprite( G.my_characters_[2].drawable_pos_x_,
                                     G.my_characters_[2].drawable_pos_y_,
                                     0, 0.5, 0.5, 0, 0);

  characters_sprites_[3].drawSprite( G.my_characters_[3].drawable_pos_x_,
                                     G.my_characters_[3].drawable_pos_y_,
                                     0, 0.5, 0.5, 0, 0);
  for (int i = 0; i < 20; ++i) {
    if (enemies[i].alive_) {
      enemies_sprites_[i].drawSprite(enemies[i].drawable_pos_x_,
                                     enemies[i].drawable_pos_y_,
                                     0, 0.5, 0.5, 0, 0);
    }
  }


  if (draw_player_stats_) seePlayerStats();
  drawcheckBattle();

  drawCurrentMovement();


}

void GameScene::drawCurrentMovement() {

  Character *players = GameManager::instance().my_characters_;
  short int& player_turn = GameManager::instance().player_turn_counter_;

  char movements[2];
  short int movement_controller = player_turn + 1;

  if (player_turn + 1 > 3) {
    movement_controller = 0;
  }

  if (players[player_turn].movements_ > 0) {
    itoa(players[player_turn].movements_, movements, 10);
  }
  else {
    itoa(players[movement_controller].movements_, movements, 10);
  }


  movements_on_screen_text_.drawText(20, 20, "Movements: ");
  movements_on_screen_number_.drawText(140, 20, movements);

}

void GameScene::drawPlayerOnMap() {

  GameManager& G = GameManager::instance();

  switch (movement_direction_) {

    case 1:
      //UP
      // if (checkMove()) {
      G.my_characters_[G.player_turn_counter_].drawable_pos_y_ -= 32;
      //  }

      break;
    case 2:
      //DOWN
      //  if (checkMove()) {
      G.my_characters_[G.player_turn_counter_].drawable_pos_y_ += 32;
      //  }
      break;

    case 3:
      //RIGHT
      //  if (checkMove()) {
      G.my_characters_[G.player_turn_counter_].drawable_pos_x_ += 32;
      //  }
      break;

    case 4:
      //LEFT
      //    if (checkMove()) {
      G.my_characters_[G.player_turn_counter_].drawable_pos_x_ -= 32;
      //    }
      break;


  }

}

void GameScene::drawPlayerTurnIndicator() {

  Character *players = GameManager::instance().my_characters_;
  short int& player_turn = GameManager::instance().player_turn_counter_;


  float points[10];
  short int aux_turn_painter;

  aux_turn_painter = player_turn + 1;

  if (player_turn + 1 >= 4) {

    aux_turn_painter = 0;
  }

  if (players[player_turn].movements_ > 0) {
    points[0] = players[player_turn].drawable_pos_x_;
    points[1] = players[player_turn].drawable_pos_y_ + 4;

    points[2] = players[player_turn].drawable_pos_x_ + 36;
    points[3] = players[player_turn].drawable_pos_y_ + 4;

    points[4] =  players[player_turn].drawable_pos_x_ + 36;
    points[5] =  players[player_turn].drawable_pos_y_ + 36;

    points[6] = players[player_turn].drawable_pos_x_ ;
    points[7] = players[player_turn].drawable_pos_y_ + 36;

    points[8] =  players[player_turn].drawable_pos_x_;
    points[9] =  players[player_turn].drawable_pos_y_ + 4;

    ESAT::DrawSetStrokeColor(200, 50, 0, 255);
    ESAT::DrawSetFillColor(255, 215, 0, 150);
    ESAT::DrawSolidPath(points, 5, true);
  }

  else {
    points[0] = players[aux_turn_painter].drawable_pos_x_;
    points[1] = players[aux_turn_painter].drawable_pos_y_ + 4;

    points[2] = players[aux_turn_painter].drawable_pos_x_ + 36;
    points[3] = players[aux_turn_painter].drawable_pos_y_ + 4;

    points[4] =  players[aux_turn_painter].drawable_pos_x_ + 36;
    points[5] =  players[aux_turn_painter].drawable_pos_y_ + 36;

    points[6] = players[aux_turn_painter].drawable_pos_x_ ;
    points[7] = players[aux_turn_painter].drawable_pos_y_ + 36;

    points[8] =  players[aux_turn_painter].drawable_pos_x_;
    points[9] =  players[aux_turn_painter].drawable_pos_y_ + 4;

    ESAT::DrawSetStrokeColor(200, 50, 0, 255);
    ESAT::DrawSetFillColor(255, 215, 0, 150);
    ESAT::DrawSolidPath(points, 5, true);

  }






}

void GameScene::initGrid() {

  my_drawable_grid_.init(18, 25);

  FILE *my_file;

  my_file = fopen("../map.txt", "r");

  for (int i = 0; i < (18 * 25); ++i) {

    fscanf(my_file, "%d,", &my_drawable_grid_.data_[i]);
  }

  fclose(my_file);


}

void GameScene::initWalkingGrid() {

  GameManager& G = GameManager::instance();

  G.my_walking_grid_.init(18, 25);

  FILE *my_file;

  my_file = fopen("../collision_map.txt", "r");

  for (int i = 0; i < (18 * 25); ++i) {

    fscanf(my_file, "%d,", &G.my_walking_grid_.data_[i]);
  }

  fclose(my_file);


}


void GameScene::initPlayersPositions() {

  GameManager& G = GameManager::instance();

  G.my_walking_grid_.data_[186] = 1;
  //G.my_characters_[0].own_id_;
  G.my_characters_[0].array_position_ = 186;

  G.my_characters_[0].drawable_pos_x_ = 11 * 32;
  G.my_characters_[0].drawable_pos_y_ = 7 * 32;


  G.my_walking_grid_.data_[188] = 2;
  //G.my_characters_[1].own_id_;
  G.my_characters_[1].array_position_ = 188;

  G.my_characters_[1].drawable_pos_x_ = 13 * 32;
  G.my_characters_[1].drawable_pos_y_ = 7 * 32;

  G.my_walking_grid_.data_[236] = 3;
  // G.my_characters_[2].own_id_;
  G.my_characters_[2].array_position_ = 236;

  G.my_characters_[2].drawable_pos_x_ = 11 * 32;
  G.my_characters_[2].drawable_pos_y_ = 9 * 32;

  G.my_walking_grid_.data_[238] = 4;
  //  G.my_characters_[3].own_id_;
  G.my_characters_[3].array_position_ = 238;

  G.my_characters_[3].drawable_pos_x_ = 13 * 32;
  G.my_characters_[3].drawable_pos_y_ = 9 * 32;


}

void GameScene::initEnemiesPositions() {

  Character *enemies = GameManager::instance().enemies_;
  Grid& grid = GameManager::instance().my_walking_grid_;

  ///////////////////////////////////////////////////
  //RIGHT - EASY
  grid.data_[197] = enemies[0].own_id_;
  enemies[0].array_position_ = 197;
  enemies[0].drawable_pos_x_ = 22 * 32;
  enemies[0].drawable_pos_y_ = 7 * 32;

  grid.data_[222] = enemies[1].own_id_;
  enemies[1].array_position_ = 222;
  enemies[1].drawable_pos_x_ = 22 * 32;
  enemies[1].drawable_pos_y_ = 8 * 32;

  grid.data_[247] = enemies[2].own_id_;
  enemies[2].array_position_ = 247;
  enemies[2].drawable_pos_x_ = 22 * 32;
  enemies[2].drawable_pos_y_ = 9 * 32;
  ////////////////////////////////////////////////
  //DOWN - EASY
  grid.data_[386] = enemies[3].own_id_;
  enemies[3].array_position_ = 386;
  enemies[3].drawable_pos_x_ = 11 * 32;
  enemies[3].drawable_pos_y_ = 15 * 32;

  grid.data_[387] = enemies[4].own_id_;
  enemies[4].array_position_ = 387;
  enemies[4].drawable_pos_x_ = 12 * 32;
  enemies[4].drawable_pos_y_ = 15 * 32;

  grid.data_[388] = enemies[5].own_id_;
  enemies[5].array_position_ = 388;
  enemies[5].drawable_pos_x_ = 13 * 32;
  enemies[5].drawable_pos_y_ = 15 * 32;

  ///////////////////////////////////////////////
  //LEFT - EASY
  grid.data_[177] = enemies[6].own_id_;
  enemies[6].array_position_ = 177;
  enemies[6].drawable_pos_x_ = 2 * 32;
  enemies[6].drawable_pos_y_ = 7 * 32;

  grid.data_[202] = enemies[7].own_id_;
  enemies[7].array_position_ = 202;
  enemies[7].drawable_pos_x_ = 2 * 32;
  enemies[7].drawable_pos_y_ = 8 * 32;

  grid.data_[227] = enemies[8].own_id_;
  enemies[8].array_position_ = 227;
  enemies[8].drawable_pos_x_ = 2 * 32;
  enemies[8].drawable_pos_y_ = 9 * 32;

  ///////////////////////////////////////////////
  //UP - EASY
  grid.data_[61] = enemies[9].own_id_;
  enemies[9].array_position_ = 61;
  enemies[9].drawable_pos_x_ = 11 * 32;
  enemies[9].drawable_pos_y_ = 2 * 32;

  grid.data_[62] = enemies[10].own_id_;
  enemies[10].array_position_ = 62;
  enemies[10].drawable_pos_x_ = 12 * 32;
  enemies[10].drawable_pos_y_ = 2 * 32;

  grid.data_[63] = enemies[11].own_id_;
  enemies[11].array_position_ = 63;
  enemies[11].drawable_pos_x_ = 13 * 32;
  enemies[11].drawable_pos_y_ = 2 * 32;
  //////////////////////////////////////////////
  //RIGHT - MEDIUM

  grid.data_[223] = enemies[12].own_id_;
  enemies[12].array_position_ = 223;
  enemies[12].drawable_pos_x_ = 23 * 32;
  enemies[12].drawable_pos_y_ = 8 * 32;

  //////////////////////////////////////////////
  //DOWN - MEDIUM
  grid.data_[412] = enemies[13].own_id_;
  enemies[13].array_position_ = 412;
  enemies[13].drawable_pos_x_ = 12 * 32;
  enemies[13].drawable_pos_y_ = 16 * 32;
  //////////////////////////////////////////////
  //LEFT - MEDIUM
  grid.data_[201] = enemies[14].own_id_;
  enemies[14].array_position_ = 201;
  enemies[14].drawable_pos_x_ = 1 * 32;
  enemies[14].drawable_pos_y_ = 8 * 32;
  //////////////////////////////////////////////
  //UP - MEDIUM
  grid.data_[37] = enemies[15].own_id_;
  enemies[15].array_position_ = 37;
  enemies[15].drawable_pos_x_ = 12 * 32;
  enemies[15].drawable_pos_y_ = 1 * 32;
  //////////////////////////////////////////////
  //RIGTH - HARD
  grid.data_[224] = enemies[16].own_id_;
  enemies[16].array_position_ = 224;
  enemies[16].drawable_pos_x_ = 24 * 32;
  enemies[16].drawable_pos_y_ = 8 * 32;
  //////////////////////////////////////////////
  //DOWN - HARD
  grid.data_[437] = enemies[17].own_id_;
  enemies[17].array_position_ = 437;
  enemies[17].drawable_pos_x_ = 12 * 32;
  enemies[17].drawable_pos_y_ = 17 * 32;
  //////////////////////////////////////////////
  //LEFT - HARD
  grid.data_[200] = enemies[18].own_id_;
  enemies[18].array_position_ = 200;
  enemies[18].drawable_pos_x_ = 0 * 32;
  enemies[18].drawable_pos_y_ = 8 * 32;
  //////////////////////////////////////////////
  //UP - HARD
  grid.data_[12] = enemies[19].own_id_;
  enemies[19].array_position_ = 12;
  enemies[19].drawable_pos_x_ = 12 * 32;
  enemies[19].drawable_pos_y_ = 0 * 32;

}

void drawInitEnemiesPositions() {


}

void GameScene::seePlayerStats() {

  GameManager& G = GameManager::instance();
  Character *players = GameManager::instance().my_characters_;
  Grid& grid = GameManager::instance().my_walking_grid_;
  short int& player_turn = GameManager::instance().player_turn_counter_;

  float points[10];
  char heal_p[4], magic_p[4], attack_p[4], defense_p[4],
       dodge_p[4], level[4], movements[4], name[32];

  itoa(players[player_turn].heal_points_, heal_p, 10);
  itoa(players[player_turn].magic_points_, magic_p, 10);
  itoa(players[player_turn].attack_power_, attack_p, 10);
  itoa(players[player_turn].defense_power_, defense_p, 10);
  itoa(players[player_turn].dodge_power_, dodge_p, 10);
  itoa(players[player_turn].current_level_, level, 10);
  itoa(players[player_turn].movements_, movements, 10);
  //itoa(players[player_turn].name_, name, 10);

  points[0] =  250;
  points[1] =  70;

  points[2] =  550;
  points[3] =  70;

  points[4] =  550;
  points[5] =  400;

  points[6] =  250;
  points[7] =  400;

  points[8] =  250;
  points[9] =  70;

  ESAT::DrawSetStrokeColor(0, 200, 0, 255);
  ESAT::DrawSetFillColor(100, 200, 0, 150);
  ESAT::DrawSolidPath(points, 5, true);

  ESAT::DrawSetFillColor(0, 0, 0, 150);

  character_stats_texts_[0].drawText(300, 140, "Heal: ");
  character_stats_texts_[1].drawText(300, 180, "Mana: ");
  character_stats_texts_[2].drawText(300, 220, "Attack: ");
  character_stats_texts_[3].drawText(300, 260, "Defense: ");
  character_stats_texts_[4].drawText(300, 300, "Dodge: ");
  character_stats_texts_[5].drawText(300, 340, "Level: ");
  character_stats_texts_[6].drawText(300, 380, "Movements: ");



  character_stats_numbers_[0].drawText(450, 140, heal_p);
  character_stats_numbers_[1].drawText(450, 180, magic_p );
  character_stats_numbers_[2].drawText(450, 220, attack_p);
  character_stats_numbers_[3].drawText(450, 260, defense_p);
  character_stats_numbers_[4].drawText(450, 300, dodge_p);
  character_stats_numbers_[5].drawText(450, 340, level);
  character_stats_numbers_[6].drawText(450, 380, movements);
  character_stats_numbers_[7].drawText(350, 100, players[player_turn].name_);
}

void GameScene::applyZoom(int direction) {
  printf("%f\n", zoom_ );
  if (zoom_ >= 0) {
    zoom_ += (0.1) * direction;
  }
  else {
    zoom_ = 0;
  }

}