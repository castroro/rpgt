
#include "character.h"
#include <string.h>

Character::Character() {

  alive_ = false;
  shopping_ = false;
  character_has_magic_ = false;
  is_moving_ = false;

  heal_points_ = 0;
  max_heal_points_ = 0;
  magic_points_ = 0;
  attack_power_ = 0;
  defense_power_ = 0;
  magic_attack_power_ = 0;
  magic_defense_power_ = 0;
  dodge_power_ = 0;
  max_magic_points_ = 0;

  current_level_ = 1;
  experience_ = 0;
  items_ = nullptr;

  movements_ = 0;

  drawable_pos_x_ = 0;
  drawable_pos_y_ = 0;
  array_position_ = 0;
  array_id_ = 1;

  own_id_ = 0;

  gold_ = 0;

  is_attacking_ = false;
  battle_posibility_ = false;

}

Character::~Character() {}

//void Character::chooseType(int type) {
void Character::chooseType(CharacterType type, short int own_id, char name[32]) {

  switch (type) {

    case kCharacter_Warrior:
      alive_ = true;
      heal_points_ = 130;
      max_heal_points_ = 130;

      max_magic_points_ = 0;
      magic_points_ = 0;

      attack_power_ = 10;
      defense_power_ = 5;
      dodge_power_ = 1;

      magic_attack_power_ = 0;
      magic_defense_power_ = 1;

      movements_ = 6;
      movements_reset_ = movements_;

      enum_type_ = kCharacter_Warrior;

      character_has_magic_ = false;

      own_id_ = own_id;
      array_id_ = own_id;

      strcpy(name_, name);


      break;

    case kCharacter_Roge:
      alive_ = true;
      heal_points_ = 100;
      max_heal_points_ = 100;

      max_magic_points_ = 20;
      magic_points_ = 20;

      attack_power_ = 6;
      defense_power_ = 3;
      dodge_power_ = 3;

      magic_attack_power_ = 6;
      magic_defense_power_ = 3;

      movements_ = 10;
      movements_reset_ = movements_;

      enum_type_ = kCharacter_Roge;

      character_has_magic_ = true;

      own_id_ = own_id;
      array_id_ = own_id;
      strcpy(name_, name);
      break;

    case kCharacter_Mage:
      alive_ = true;
      heal_points_ = 50;
      max_heal_points_ = 50;

      max_magic_points_ = 80;
      magic_points_ = 80;

      attack_power_ = 4;
      defense_power_ = 3;
      dodge_power_ = 3;

      magic_attack_power_ = 6;
      magic_defense_power_ = 3;


      movements_ = 6;
      movements_reset_ = movements_;

      enum_type_ = kCharacter_Mage;

      character_has_magic_ = true;

      own_id_ = own_id;
      array_id_ = own_id;
      strcpy(name_, name);
      break;

    case kChatacter_Healer:
      alive_ = true;
      heal_points_ = 60;
      max_heal_points_ = 60;

      max_magic_points_ = 60;
      magic_points_ = 60;

      attack_power_ = 3;
      defense_power_ = 3;
      dodge_power_ = 2;

      magic_attack_power_ = 6;
      magic_defense_power_ = 3;


      movements_ = 6;
      movements_reset_ = movements_;

      enum_type_ = kChatacter_Healer;

      character_has_magic_ = true;

      own_id_ = own_id;

      array_id_ = own_id;
      strcpy(name_, name);
      break;

    case kCharacter_SwordMan:
      alive_ = true;
      heal_points_ = 200;
      max_heal_points_ = 200;

      max_magic_points_ = 20;
      magic_points_ = 20;

      attack_power_ = 16;
      defense_power_ = 3;
      dodge_power_ = 3;

      magic_attack_power_ = 6;
      magic_defense_power_ = 3;

      movements_ = 12;
      movements_reset_ = movements_;

      enum_type_ = kCharacter_SwordMan;

      character_has_magic_ = false;

      own_id_ = own_id;
      array_id_ = own_id;
      strcpy(name_, name);
      break;


    default:

      break;
  }
}

void Character::magicPowers(CharacterType type) {

  /*
   switch (the_character->character_type_) {

     case kCharacter_Warrior:

       break;

     case kCharacter_Roge:

       break;

     case kCharacter_Mage:



       break;

     case kCharacter_Healer:



       break;

   case default:
       printf("an error ocurred in the magic powers function");

       break;



   }

   */




}

// GUSTAVO: Why don't you use the CharacterType enum
void Character::levelUp(short int type) {


  if (experience_ >= experience_to_upgrade_level) {

    switch (type) {

      case 1:

        current_level_ += 1;
        heal_points_ += 60;
        max_heal_points_ += 60;

        max_magic_points_ += 0;
        magic_points_ += 0;

        attack_power_ += 4;
        defense_power_ += 3;
        dodge_power_ += 1;

        magic_attack_power_ += 0;
        magic_defense_power_ += 0;

        movements_ += 2;
        movements_reset_ += 2;

        experience_ = 0;
        experience_to_upgrade_level =
          experience_to_upgrade_level + (current_level_ * 2);

        break;

      case 2:

        current_level_ += 1;
        heal_points_ += 60;
        max_heal_points_ += 60;

        max_magic_points_ += 0;
        magic_points_ += 0;

        attack_power_ += 4;
        defense_power_ += 3;
        dodge_power_ += 1;

        magic_attack_power_ += 0;
        magic_defense_power_ += 0;

        movements_ += 2;
        movements_reset_ += 2;

        experience_ = 0;
        experience_to_upgrade_level =
          experience_to_upgrade_level + (current_level_ * 2);

        break;

      case 3:
        current_level_ += 1;
        heal_points_ += 60;
        max_heal_points_ += 60;

        max_magic_points_ += 0;
        magic_points_ += 0;

        attack_power_ += 4;
        defense_power_ += 3;
        dodge_power_ += 1;

        magic_attack_power_ += 0;
        magic_defense_power_ += 0;

        movements_ += 2;
        movements_reset_ += 2;

        experience_ = 0;
        experience_to_upgrade_level =
          experience_to_upgrade_level + (current_level_ * 2);



        break;

      case 4:
        current_level_ += 1;
        heal_points_ += 60;
        max_heal_points_ += 60;

        max_magic_points_ += 0;
        magic_points_ += 0;

        attack_power_ += 4;
        defense_power_ += 3;
        dodge_power_ += 1;

        magic_attack_power_ += 0;
        magic_defense_power_ += 0;

        movements_ += 2;
        movements_reset_ += 2;

        experience_ = 0;
        experience_to_upgrade_level =
          experience_to_upgrade_level + (current_level_ * 2);



        break;

      case 5:
        current_level_ += 1;
        heal_points_ += 60;
        max_heal_points_ += 60;

        max_magic_points_ += 0;
        magic_points_ += 0;

        attack_power_ += 4;
        defense_power_ += 3;
        dodge_power_ += 1;

        magic_attack_power_ += 0;
        magic_defense_power_ += 0;

        movements_ += 2;
        movements_reset_ += 2;

        experience_ = 0;
        experience_to_upgrade_level =
          experience_to_upgrade_level + (current_level_ * 2);



        break;
        /*
            case default:
                printf("an error ocurred in the magic powers function");

                break;

        */

    }

  }
}

void Character::createTypeEnemy(EnemyType type , short int own_id) {

  switch (type) {

    case kEnemy_easy:
      alive_ = true;
      heal_points_ = 20;
      max_heal_points_ = 20;

      attack_power_ = 3;
      defense_power_ = 1;
      dodge_power_ = 1;

      movements_ = 8;
      movements_reset_ = movements_;

      enum_type_ = kEnemy_easy;

      character_has_magic_ = false;

      array_id_ = 2;
      own_id_ = own_id;
      gold_ = 20;
      experience_to_upgrade_level = 100;

      break;

    case kEnemy_medium:
      heal_points_ = 40;
      max_heal_points_ = 40;
      alive_ = true;
      attack_power_ = 6;
      defense_power_ = 2;
      dodge_power_ = 1;

      movements_ = 6;
      movements_reset_ = movements_;

      enum_type_ = kEnemy_medium;

      character_has_magic_ = false;

      array_id_ = 3;
      own_id_ = own_id;
      gold_ = 40;
      experience_to_upgrade_level = 100;

      break;

    case kEnemy_hard:
      heal_points_ = 60;
      max_heal_points_ = 60;
      alive_ = true;
      attack_power_ = 8;
      defense_power_ = 4;
      dodge_power_ = 1;

      movements_ = 6;
      movements_reset_ = movements_;

      enum_type_ = kEnemy_hard;

      character_has_magic_ = false;

      array_id_ = 4;
      own_id_ = own_id;
      gold_ = 60;
      experience_to_upgrade_level = 100;

      break;

    case kEnemy_boss:

      heal_points_ = 150;
      max_heal_points_ = 150;
      alive_ = true;
      attack_power_ = 10;
      defense_power_ = 6;
      dodge_power_ = 3;

      movements_ = 3;
      movements_reset_ = movements_;

      enum_type_ = kEnemy_boss;

      character_has_magic_ = false;

      array_id_ = 5;
      own_id_ = own_id;
      gold_ = 80;
      experience_to_upgrade_level = 100;


      break;



  }


}


void Character::resetMovement() {

  movements_ = movements_reset_;
}
