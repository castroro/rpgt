/*
 *
 * Author: Alejandro Castro Royo <castroro@esat-alumni.com>
 *
 */

#ifndef __MY_SCENE_SELECTOR_H__
#define __MY_SCENE_SELECTOR_H__ 1


#include "init_scene.h"
#include "choose_team_scene.h"
#include "game_scene.h"
#include "combat_scene.h"

/**
 * @brief      Class for scene selector.
 *
 */

class SceneSelector {

    public:

        /**
         * @brief      { Scene constructor }
         */
        SceneSelector();
        ~SceneSelector() {};

/**
 * @brief      { Start all the inits of the game scenes }
 */
        void init();
        /**
         * @brief      { Changes the indicate input }
         */
        void input();
        //
        // @brief      { Changes the indicate update }
        //
        void update();
        //
        // @brief      { Changes the indicate draw }
        //
        void draw();



        InitScene intro_scene_;
        ChooseTeamScene choose_team_scene_;
        GameScene game_scene_;
        CombatScene combat_scene_;

        bool end_of_game_;


    private:



};


#endif