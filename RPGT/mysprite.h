/*
 *
 * Author: Alejandro Castro Royo <castroro@esat-alumni.com>
 *
 */
#ifndef __MY_SPRITE__
#define __MY_SPRITE__ 1

#include <ESAT/sprite.h>



/*
- Con un atributo que represente su SpriteHandle (e.g: handle_)
- Con un atributo que represente su SpriteTransform (e.g: trans_)
- Con un constructor que lo inicializa a partir de un nombre de fichero
- Con un constructor que lo inicializa a partir de un handle ya existente
- Con un constructor que lo inicializa a partir de un bloque de memoria
- Con un destructor que libera la memoria usada (i.e. Release)
- Con un método que lo actualiza a partir de un bloque de memoria
- Con dos métodos para averiguar el ancho y alto del Sprite
- Con un método que averigüe el color de un pixel concreto
- Con un método Draw() que lo pinte

*/


/**
 * @brief      Class for , to implement all the sprites of the game.
 */

class Sprite {

    public:
/**
 * @brief      { constructor of the class }
 */
        Sprite();

/**
 * @brief      Destroys the object sprite.
 */
        ~Sprite();
/**
 * @brief      { inits the sprite }
 *
 * @param[in]  path  The direction of the image
 */
        void spriteInit(const char *path);
        /**
         * @brief      { knows the height of the sprite }
         *
         * @return     { height value of the sprite }
         */
        int spriteHeight();
        /**
         * @brief      { knows the width of the sprite }
         *
         * @return     {  width value of the sprite }
         */
        int spriteWidth();
        //
        // @brief      { Get the pixel of the sprite }
        //
        // @param[in]  x        { width }
        // @param[in]  y        { height }
        // @param      outRGBA  The out rgba color of the pixel in char
        //
        void spriteGetPixel(int x, int y, unsigned char outRGBA[4]);
//
// @brief      Draws a sprite.
//
// @param[in]  position_x       The position x respect the screen
// @param[in]  position_y       The position y respect the screen
// @param[in]  angle            The angle respect the screen
// @param[in]  scale_x          The scale x respect the screen
// @param[in]  scale_y          The scale y respect the screen
// @param[in]  sprite_origin_x  The sprite origin x respect the screen
// @param[in]  sprite_origin_y  The sprite origin y respect the screen
//
        void drawSprite(float position_x, float position_y, float angle,
                        float scale_x, float scale_y, float sprite_origin_x,
                        float sprite_origin_y);


        ESAT::SpriteHandle my_handle_;
        ESAT::SpriteTransform my_transform_;


    private:
//
// @brief      { other tipe of constructor }
//
// @param[in]  path  The path
//
        Sprite(const char *path);
        //
        // @brief      { other tipe of constructor }
        //
        // @param[in]  width      The width
        // @param[in]  height     The height
        // @param[in]  data_RGBA  The data rgba
        //
        Sprite(int width, int height, const unsigned char *data_RGBA);




};

#endif

