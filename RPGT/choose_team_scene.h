/*
 *
 * Author: Alejandro Castro Royo <castroro@esat-alumni.com>
 *
 */

#ifndef __MY_TEAM_CHOOSER_H__
#define __MY_TEAM_CHOOSER_H__ 1


#include "game_manager.h"

#include "input.h"
#include "mysprite.h"
#include "mytext.h"

class ChooseTeamScene {

    public:

        /**
         * @brief      { constructor of this scene }
         */
        ChooseTeamScene();
        ~ChooseTeamScene();
        /**
         * @brief      { Character image to show}
         */
        void spriteToShow();

        void nameToShow();
        /**
         * @brief      { Description text of each character type }
         */
        void textToShow();

        void drawNumberPlayersChosed();
        /**
         * @brief      { Input of the Scene }
         */
        void input();
        /**
         * @brief      { More secure init proccess than a constructor }
         */
        void init();
        /**
         * @brief      { Update function }
         */
        void update();
        /**
         * @brief      { Show at the screen what update orders }
         */
        void draw();

        bool init_completed_;

        int drawer_delimitator_;
        int name_drawer_delimitator_;
        short int player_counter_;
        Input my_input_;
        Text characters_names_[4];
        Text player_chosed_;
        Text player_chosed_number_;
        Text my_current_text_;
        Text player_text_name_;
        Text arrows_;
        ///////
        Text sword_man_text_;
        Text sword_man_name_;
        ///////
        char name1[32], name2[32], name3[32], name4[32], name5[32], name6[32], name7[32];
        Sprite characters_sprites_[4];
        Sprite super_potato_delimitator_;
        char aux_name_[32];


    private:

};

#endif