
/*
 *
 * Author: Alejandro Castro Royo <castroro@esat-alumni.com>
 *
 */

#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__ 1

#include "game_manager.h"
#include "input.h"
#include "mysprite.h"
#include "mytext.h"

class GameScene {
    public:

        GameScene();
        ~GameScene();


        /**
         * @brief      { preapres all the variables for the scene }
         */
        void init();
        /**
         * @brief      { check the keys that are pressed }
         */
        void input();
        /**
         * @brief      { makes changes with the keys presseds }
         */
        void update();

        /**
         * @brief      { draws the sprites in the correct mode that update orders }
         */
        void draw();
        /**
         * @brief      { change the character position of the grid }
         */
        void movePlayerOnGrid();
        /**
         * @brief      Draws a player on map.
         */
        void drawPlayerOnMap();
        /**
         * @brief      { init the values of the grid }
         */
        void initGrid();
        //
        // @brief      { init the values of the walking grid  reading from a file}
        //
        void initWalkingGrid();
        /**
         * @brief      { start the player positions in the grid }
         */
        void initPlayersPositions();
        /**
         * @brief      {  start the enemies positions in the grid }
         */
        void initEnemiesPositions();
        /**
         * @brief      Draws the first position of the enemies on the grid.
         */
        void drawInitEnemiesPositions();
        //
        // @brief      { Show the player stats  }
        //
        void seePlayerStats();
        //
        // @brief      Draws a player turn indicator (color box).
        //
        void drawPlayerTurnIndicator();

        void drawCurrentMovement();
        void checkBattle();
        void movementLogic();

        bool checkMove();
        void drawcheckBattle();
        void applyZoom(int direction);

        Input my_input_;



        int movement_direction_;
        int battle_indicator_;
        int number_of_grid_rows_;
        int number_of_grid_cols_;



        Grid my_drawable_grid_;
        ////////////////////////////////
        Sprite map_sprite_;
        Sprite characters_sprites_[4];
        Sprite enemies_sprites_[20];
        /////////////////////////////////
        Text character_stats_numbers_[8];
        Text character_stats_texts_[8];
        /////////////////////////////////
        Text movements_on_screen_number_;
        Text movements_on_screen_text_;
        /////////////////////////////////

        bool enter_battle_;
        bool enter_shop_;
        bool init_completed_;

        bool its_an_enemy_;

        bool draw_player_stats_;

        float zoom_;
        float characters_zoom_;



    private:



};



#endif;