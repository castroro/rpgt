
#include "mysprite.h"

Sprite::Sprite() {}
Sprite::Sprite(const char *path) {

  my_transform_.angle = 0;
  my_transform_.scale_x = 0;
  my_transform_.scale_y = 0;
  my_transform_.x = 0;
  my_transform_.y = 0;
  my_transform_.sprite_origin_x = 0;
  my_transform_.sprite_origin_y = 0;

  my_handle_ = ESAT::SpriteFromFile(path);

}
Sprite::Sprite(int width, int height, const unsigned char *data_RGBA) {

  my_handle_ = ESAT::SpriteFromMemory(width, height, data_RGBA);

}

Sprite::~Sprite() {

  //ESAT::SpriteRelease(my_handle_);
}

void Sprite::spriteInit(const char *path) {

  my_transform_.angle = 0;
  my_transform_.scale_x = 1;
  my_transform_.scale_y = 1;
  my_transform_.x = 0;
  my_transform_.y = 0;
  my_transform_.sprite_origin_x = 0;
  my_transform_.sprite_origin_y = 0;

  my_handle_ = ESAT::SpriteFromFile(path);



}

int Sprite::spriteHeight() {

  return ESAT::SpriteHeight(my_handle_);

}

int Sprite::spriteWidth() {

  return ESAT::SpriteWidth(my_handle_);
}

void Sprite::spriteGetPixel(int x, int y, unsigned char outRGBA[4]) {

  ESAT::SpriteGetPixel(my_handle_, x, y, outRGBA);
}

void Sprite::drawSprite(float position_x, float position_y, float angle,
                        float scale_x, float scale_y, float sprite_origin_x,
                        float sprite_origin_y) {

  my_transform_.x = position_x;
  my_transform_.y = position_y;
  my_transform_.angle = angle;
  my_transform_.scale_x = scale_x;
  my_transform_.scale_y = scale_y;
  my_transform_.sprite_origin_x = sprite_origin_x;
  my_transform_.sprite_origin_y = sprite_origin_y;

  //ESAT::DrawSprite(my_handle_, position_x, position_y);
  ESAT::DrawSprite(my_handle_, my_transform_);


}


/*
#include "mysprite.h"

Sprite::Sprite() {}
Sprite::Sprite(const char *path) {

  my_transform_.angle = 0;
  my_transform_.scale_x = 0;
  my_transform_.scale_y = 0;
  my_transform_.x = 0;
  my_transform_.y = 0;
  my_transform_.sprite_origin_x = 0;
  my_transform_.sprite_origin_y = 0;

  my_handle_ = ESAT::SpriteFromFile(path);

}
Sprite::Sprite(int width, int height, const unsigned char *data_RGBA) {

  my_handle_ = ESAT::SpriteFromMemory(width, height, data_RGBA);

}

Sprite::~Sprite() {

  //ESAT::SpriteRelease(my_handle_);
}

void Sprite::spriteInit(const char *path) {

  my_transform_.angle = 0;
  my_transform_.scale_x = 1;
  my_transform_.scale_y = 1;
  my_transform_.x = 0;
  my_transform_.y = 0;
  my_transform_.sprite_origin_x = 0;
  my_transform_.sprite_origin_y = 0;

  my_handle_ = ESAT::SpriteFromFile(path);



}

int Sprite::spriteHeight() {

  return ESAT::SpriteHeight(my_handle_);

}

int Sprite::spriteWidth() {

  return ESAT::SpriteWidth(my_handle_);
}

void Sprite::spriteGetPixel(int x, int y, unsigned char outRGBA[4]) {

  ESAT::SpriteGetPixel(my_handle_, x, y, outRGBA);
}

void Sprite::drawSprite(float position_x, float position_y, float angle,
                        float scale_x, float scale_y, float sprite_origin_x,
                        float sprite_origin_y) {

  my_transform_.x = position_x;
  my_transform_.y = position_y;
  my_transform_.angle = angle;
  my_transform_.scale_x = scale_x;
  my_transform_.scale_y = scale_y;
  my_transform_.sprite_origin_x = sprite_origin_x;
  my_transform_.sprite_origin_y = sprite_origin_y;

  //ESAT::DrawSprite(my_handle_, position_x, position_y);
  ESAT::DrawSprite(my_handle_, my_transform_);


}



*/