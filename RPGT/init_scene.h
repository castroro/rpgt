/*
 *
 * Author: Alejandro Castro Royo <castroro@esat-alumni.com>
 *
 */


#ifndef __MY_INIT_SCENE_H__
#define __MY_INIT_SCENE_H__ 1

#include "game_manager.h"

#include "input.h"
#include "mysprite.h"
#include "mytext.h"

class InitScene {

    public:
        /**
         * @brief      { constructor of init scene }
         */
        InitScene();
        //
        // @brief      Destroys the object.
        //
        ~InitScene();

        void init();
        //
        // @brief      { check if any key is pressed }
        //
        void theInput();
        /**
         * @brief      { updates the status of the scene }
         */
        void update();
        //
        // @brief      { draws the current sprites at the screen }
        //
        void draw();

        bool init_completed_;
        Input my_input_;
        Text intro_text_;
        Sprite my_school_logo_;
        Sprite my_name_awesome_logo_;



};

#endif