
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "grid.h"


Grid::Grid() {

  data_ = nullptr;
  rows_ = 0;
  cols_ = 0;


}

Grid::~Grid() {
  // GUSTAVO: Check that data_ is not nullptr
  free(data_);


}


void Grid::init(int rows, int cols) {

  rows = checkInt(rows);
  cols = checkInt(cols);

  data_ = (int *) malloc(rows * cols * sizeof(int));

  rows_ = rows;
  cols_ = cols;

}

void Grid::initWithValue(int rows, int cols, int value) {

  rows = checkInt(rows);
  cols = checkInt(cols);

  rows_ = rows;
  cols_ = cols;

  data_ = (int *) malloc(rows * cols * sizeof(int));


  //memset(data_, value,  rows_ * cols_ * sizeof(int));
  for (int i = 0; i <= (cols_ * rows_); ++i) {
    data_[i] = value;
  }

}

void Grid::zeros() {

  //sizeof(int) * it is necessary?
  memset(data_, 0,  rows_ * cols_ * sizeof(int));

}

int Grid::getElement(int row, int col) {
  row = checkInt(row);
  col = checkInt(col);

  return data_[(row * cols_) + col];
}


int Grid::getElement2(int data) {
  printf("%d", data);
  return data_[data];
}


void Grid::setElement(int row, int col, int value) {

  row = checkInt(row);
  col = checkInt(col);
  value = checkInt(value);

  data_[(row * cols_) + col] = value;


}

void Grid::print() {

  for (int i = 1; i < cols_ * rows_; ++i) {
    printf(" %03d ", data_[i]);

    if (i % cols_ == 0) {
      printf("\n");
    }
  }

}

Grid Grid::add (Grid *auxGrid) {



  return *auxGrid;
}

Grid Grid::substract (Grid *auxGrid) {

  return *auxGrid;
}

int Grid::checkInt(int number) {

  if (number < 0) {

    number = (number) * (-1);
  }

  return number;
}
