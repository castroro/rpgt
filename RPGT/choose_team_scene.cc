
#include "choose_team_scene.h"
#include <string.h>

ChooseTeamScene::ChooseTeamScene() {

  init_completed_ = false;
  name_drawer_delimitator_ = 1;
  my_input_.resetInput();
}

ChooseTeamScene::~ChooseTeamScene() {


}



void ChooseTeamScene::init() {

  drawer_delimitator_ = 1;
  player_counter_ = 0;
  init_completed_ = true;
  my_current_text_.init("../fonts/endor.ttf", 20, 0, 0, 255, 0, 255);
  characters_names_[0].init("../fonts/augusta.ttf", 20, 0, 0, 0, 0, 255);
  characters_names_[1].init("../fonts/augusta.ttf", 20, 0, 0, 0, 0, 255);
  characters_names_[2].init("../fonts/augusta.ttf", 20, 0, 0, 0, 0, 255);
  characters_names_[3].init("../fonts/augusta.ttf", 20, 0, 0, 0, 0, 255);
  characters_names_[3].init("../fonts/augusta.ttf", 20, 0, 0, 0, 0, 255);
  //----------------------------------------------------------------------
  characters_sprites_[0].spriteInit("../Posibles/Arte/player/warrior.png");
  characters_sprites_[1].spriteInit("../Posibles/Arte/player/rogue.png");
  characters_sprites_[2].spriteInit("../Posibles/Arte/player/mage.png");
  characters_sprites_[3].spriteInit("../Posibles/Arte/player/healer.png");

  player_chosed_.init("../fonts/augusta.ttf", 20, 0, 0, 0, 0, 255);

  player_text_name_.init("../fonts/augusta.ttf", 20, 0, 0, 0, 0, 255);

  sword_man_text_.init("../fonts/augusta.ttf", 20, 0, 0, 0, 0, 255);
  sword_man_name_.init("../fonts/augusta.ttf", 20, 0, 0, 0, 0, 255);

  super_potato_delimitator_.spriteInit("../posibles/arte/init/poTATO.PNG");

  strcpy(name1, "Tupac");
  strcpy(name2, "Julianus");
  strcpy(name3, "PincheGuanche");
  strcpy(name4, "Ernesto");
  strcpy(name5, "Rupert");
  strcpy(name6, "Maik");
  strcpy(name7, "Gusiluz");

  arrows_.init("../fonts/augusta.ttf", 40, 0, 255, 255, 0, 255);




}

void ChooseTeamScene::input() {

  my_input_.update();


}

void ChooseTeamScene::update() {
  //----------------------------------------
  if (my_input_.arrow_up_) {
    drawer_delimitator_--;
    if (drawer_delimitator_ <= 0) {
      drawer_delimitator_ = 5;
    }
  }
  //-----------------------------------------
  if (my_input_.arrow_down_) {
    drawer_delimitator_++;
    if (drawer_delimitator_ >  5) {
      drawer_delimitator_ = 1;
    }
  }

  if (my_input_.arrow_right_) {
    name_drawer_delimitator_++;
    if (name_drawer_delimitator_ > 7) {
      name_drawer_delimitator_ = 1;
    }
  }
  //-----------------------------------------
  if (my_input_.arrow_left_) {
    name_drawer_delimitator_--;
    if (name_drawer_delimitator_ <=  0) {
      name_drawer_delimitator_ = 7;
    }
  }
  //-----------------------------------------
  if (my_input_.action_space_bar_) {

    switch (drawer_delimitator_) {

      case 1:
        GameManager::instance().my_characters_[player_counter_].
        chooseType(GameManager::instance().my_characters_[player_counter_]
                   .kCharacter_Warrior, player_counter_ + 1, aux_name_);

        break;

      case 2:
        GameManager::instance().my_characters_[player_counter_].
        chooseType(GameManager::instance().my_characters_[player_counter_]
                   .kCharacter_Roge, player_counter_ + 1, aux_name_);

        break;

      case 3:
        GameManager::instance().my_characters_[player_counter_].
        chooseType(GameManager::instance().my_characters_[player_counter_]
                   .kCharacter_Mage, player_counter_ + 1, aux_name_);

        break;

      case 4:
        GameManager::instance().my_characters_[player_counter_].
        chooseType(GameManager::instance().my_characters_[player_counter_]
                   .kChatacter_Healer, player_counter_ + 1, aux_name_);

        break;

      case 5:
        GameManager::instance().my_characters_[player_counter_].
        chooseType(GameManager::instance().my_characters_[player_counter_]
                   .kCharacter_SwordMan, player_counter_ + 1, aux_name_);

        break;
    }


    player_counter_++;

    if (player_counter_ >= 4) {
      GameManager::instance().current_scene_ =  GameManager::kScene_GameScene;
    }
  }

}

void ChooseTeamScene::spriteToShow() {


  switch (drawer_delimitator_) {

    case 1:

      characters_sprites_[0].drawSprite(300, 250, 0, 2.5, 2.5, 0, 0 );
      super_potato_delimitator_.drawSprite(50, 80, 0, 0.5, 0.5, 0, 0);


      break;

    case 2:

      characters_sprites_[1].drawSprite(300, 250, 0, 2.5, 2.5, 0, 0 );
      super_potato_delimitator_.drawSprite(50, 180, 0, 0.5, 0.5, 0, 0);

      break;

    case 3:

      characters_sprites_[2].drawSprite(300, 250, 0, 2.5, 2.5, 0, 0 );
      super_potato_delimitator_.drawSprite(50, 280, 0, 0.5, 0.5, 0, 0);

      break;

    case 4:

      characters_sprites_[3].drawSprite(300, 250, 0, 2.5, 2.5, 0, 0 );
      super_potato_delimitator_.drawSprite(50, 380, 0, 0.5, 0.5, 0, 0);

      break;

    case 5:
      characters_sprites_[1].drawSprite(300, 250, 0, 2.5, 2.5, 0, 0 );
      super_potato_delimitator_.drawSprite(50, 480, 0, 0.5, 0.5, 0, 0);

      break;
  }


}

void ChooseTeamScene::nameToShow() {

  switch (name_drawer_delimitator_) {

    case 1:
      player_text_name_.drawText(400, 200, name1);
      strcpy(aux_name_, name1);
      break;

    case 2:
      player_text_name_.drawText(400, 200, name2);
      strcpy(aux_name_, name2);
      break;


    case 3:
      player_text_name_.drawText(400, 200, name3);
      strcpy(aux_name_, name3);
      break;


    case 4:
      player_text_name_.drawText(400, 200, name4);
      strcpy(aux_name_, name4);
      break;

    case 5:
      player_text_name_.drawText(400, 200, name5);
      strcpy(aux_name_, name5);
      break;

    case 6:
      player_text_name_.drawText(400, 200, name6);
      strcpy(aux_name_, name6);
      break;

    case 7:
      player_text_name_.drawText(400, 200, name7);
      strcpy(aux_name_, name7);
      break;



  }
}

void ChooseTeamScene::textToShow() {

  switch (drawer_delimitator_) {

    case 1:
      my_current_text_.drawText(300, 100, "Most powerful melee character.");

      break;

    case 2:

      my_current_text_.drawText(300, 100, " Class with a hihg number of movements.");

      break;

    case 3:
      my_current_text_.drawText(300, 100, "Magic powers are the aca of the tomb.");


      break;

    case 4:
      my_current_text_.drawText(300, 100, "All groups need one guy with this task {or not}.");

      break;
    case 5:
      my_current_text_.drawText(300, 100, "New class for validation");
      break;
  }

  characters_names_[0].drawText( 100, 100, "Warrior");
  characters_names_[1].drawText( 100, 200, "Rogue");
  characters_names_[2].drawText( 100, 300, "Mage");
  characters_names_[3].drawText( 100, 400, "Healer");
  sword_man_name_.drawText( 100, 500, "SwordMan");

  arrows_.drawText(380, 200, "<-                    ->");



}
void ChooseTeamScene::drawNumberPlayersChosed() {

  char chosed[4];
  itoa(player_counter_, chosed, 10);

  player_chosed_.drawText(550, 540, "Number players: ");
  player_chosed_number_.drawText(780, 540, chosed);

}

void ChooseTeamScene::draw() {

  textToShow();
  spriteToShow();
  nameToShow();
  drawNumberPlayersChosed();
}