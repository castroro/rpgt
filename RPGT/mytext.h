#ifndef __MY_TEXT_H_
#define __MY_TEXT_H_ 1


#include "ESAT/draw.h"
/*
- Con dos atributos que representen su color de trazo y de relleno
- Con un atributo que represente su nombre de fuente
- Con un atributo que represente su tamaño de fuente
- Con un atributo que represente su radio de blur
- Con un atributo (o dos) que represente su posición
- Con un atributo que represente el contenido del texto
- Con un método Draw() que lo pinte
*/

/**
 * @brief      Class for text, used to implement all the texts of the game.
 */
class Text {
  public:

    Text() {};
    ~Text() {};
/**
 * @brief      { init of the texts, were all its attributes are choosed }
 *
 * @param[in]  name         The name, direction of the font wanted to use.
 * @param[in]  size         The size
 * @param[in]  blur_radius  The blur radius
 * @param[in]  red          The red
 * @param[in]  green        The green
 * @param[in]  blue         The blue
 * @param[in]  alpha        The alpha
 */
    void init(const char *name, float size, float blur_radius,
              unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha);
    /**
     * @brief      Draws a text.
     *
     * @param[in]  x     { parameter_description }
     * @param[in]  y     { parameter_description }
     * @param[in]  text  The text
     */
    void drawText(float x, float y, const char *text);




    float position_x_;
    float position_y_;
    const char *text_;

  private:

};


#endif