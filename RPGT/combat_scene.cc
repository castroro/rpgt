#include "combat_scene.h"


CombatScene::CombatScene() {

  end_combat_ = false;
  action_ = false;
  drawable_text_indicator_;
  my_input_.resetInput();

}

CombatScene::~CombatScene() {

}

void CombatScene::init() {

  end_combat_indicator_.init("../fonts/augusta.ttf", 40, 0, 255, 255, 0, 255);

  for (int i = 0; i < 4; ++i) {
    combat_texts_indicators_[i].init("../fonts/augusta.ttf", 40, 0, 255, 255, 0, 255);
  }

  combat_status_actions_texts_[0].init("../fonts/augusta.ttf", 40, 0, 255, 255, 0, 255);
  combat_status_actions_texts_[1].init("../fonts/augusta.ttf", 40, 0, 255, 255, 0, 255);
  combat_status_actions_texts_[2].init("../fonts/augusta.ttf", 40, 0, 255, 255, 0, 255);
  combat_status_actions_texts_[3].init("../fonts/augusta.ttf", 40, 0, 255, 255, 0, 255);
  combat_status_actions_texts_[4].init("../fonts/augusta.ttf", 40, 0, 255, 255, 0, 255);

  my_input_.resetInput();
  back_ground_image_.spriteInit("../posibles/arte/battle/back.png");

  character_stats_name_.init("../fonts/endor.ttf", 40, 0, 0, 0, 0, 255);
  for (int i = 0; i < 6; ++i) {
    character_stats_numbers_[i].init("../fonts/endor.ttf", 40, 0, 0, 0, 0, 255);
    character_stats_texts_[i].init("../fonts/endor.ttf", 40, 0, 0, 0, 0, 255);
    enemy_stats_numbers_[i].init("../fonts/endor.ttf", 40, 0, 0, 0, 0, 255);
    enemy_stats_texts_[i].init("../fonts/endor.ttf", 40, 0, 0, 0, 0, 255);
  }



}

void CombatScene::input() {

  my_input_.update();


}

void CombatScene::update() {

  if (GameManager::instance().my_characters_
      [GameManager::instance().player_turn_counter_].is_attacking_  = true) {

    if (my_input_.arrow_up_ && !action_) {
      attack();
    }
    //-----------------------------------------
    if (my_input_.arrow_down_ && !action_) {

      attackWithMagic();
    }

    if (my_input_.arrow_right_ && !action_) {


    }

    if (my_input_.arrow_left_ && !action_) {

      runAway();

    }
    if (my_input_.action_space_bar_) {
      end_combat_ = true;

    }


  }

  else {
    end_combat_ = true;
  }


  if (end_combat_ && action_) {

    checkEnemyDead();
    end_combat_ = false;
    GameManager::instance().my_characters_
    [GameManager::instance().player_turn_counter_].battle_posibility_ = false;

    GameManager::instance().my_characters_
    [GameManager::instance().player_turn_counter_].is_attacking_ = false;

    drawable_text_indicator_ = 0;

    action_ = false;

    GameManager::instance().battle_indicator_  = 0;

    GameManager::instance().my_characters_
    [GameManager::instance().player_turn_counter_].levelUp(GameManager::instance()
        .my_characters_[GameManager::instance().player_turn_counter_].enum_type_);

    GameManager::instance().my_characters_
    [GameManager::instance().player_turn_counter_].movements_ = 0;

    GameManager::instance().current_scene_ = GameManager::kScene_GameScene;
  }

}

void CombatScene::draw() {

  back_ground_image_.drawSprite(0, 0, 0, 1.5, 1.5, 0, 0);

  infoSquares();
  playerStats();
  enemyStats();
  drawCombatTexts();

}

void CombatScene::infoSquares() {


  float points_of_actions[10];
  float points_character_part[10];
  float points_enemy_part[10];

  points_of_actions[0] =  10;
  points_of_actions[1] =  600;

  points_of_actions[2] =  210;
  points_of_actions[3] =  600;

  points_of_actions[4] =  210;
  points_of_actions[5] =  450;

  points_of_actions[6] =  10;
  points_of_actions[7] =  450;

  points_of_actions[8] =  10;
  points_of_actions[9] =  600;

  ESAT::DrawSetStrokeColor(0, 200, 0, 255);
  ESAT::DrawSetFillColor(100, 200, 0, 150);
  ESAT::DrawSolidPath(points_of_actions, 5, true);


  float points_of_texts[10];

  points_of_texts[0] =  210;
  points_of_texts[1] =  600;

  points_of_texts[2] =  610;
  points_of_texts[3] =  600;

  points_of_texts[4] =  610;
  points_of_texts[5] =  450;

  points_of_texts[6] =  210;
  points_of_texts[7] =  450;

  points_of_texts[8] =  210;
  points_of_texts[9] =  600;

  ESAT::DrawSetStrokeColor(0, 200, 0, 255);
  ESAT::DrawSetFillColor(0, 50, 50, 150);
  ESAT::DrawSolidPath(points_of_texts, 5, true);

  points_character_part[0] =  10;
  points_character_part[1] =  70;

  points_character_part[2] =  160;
  points_character_part[3] =  70;

  points_character_part[4] =  160;
  points_character_part[5] =  310;

  points_character_part[6] =  10;
  points_character_part[7] =  310;

  points_character_part[8] =  10;
  points_character_part[9] =  70;

  ESAT::DrawSetStrokeColor(0, 200, 0, 255);
  ESAT::DrawSetFillColor(100, 200, 0, 150);
  ESAT::DrawSolidPath(points_character_part, 5, true);
  ESAT::DrawSetFillColor(0, 0, 0, 150);

  points_enemy_part[0] =  680;
  points_enemy_part[1] =  110;

  points_enemy_part[2] =  800;
  points_enemy_part[3] =  110;

  points_enemy_part[4] =  800;
  points_enemy_part[5] =  300;

  points_enemy_part[6] =  680;
  points_enemy_part[7] =  300;

  points_enemy_part[8] =  680;
  points_enemy_part[9] =  110;

  ESAT::DrawSetStrokeColor(0, 0, 0, 255);
  ESAT::DrawSetFillColor(100, 0, 100, 180);
  ESAT::DrawSolidPath(points_enemy_part, 5, true);
  ESAT::DrawSetFillColor(0, 0, 0, 150);



}

void CombatScene::playerStats() {

  GameManager& G = GameManager::instance();
  Character *players = GameManager::instance().my_characters_;
  Grid& grid = GameManager::instance().my_walking_grid_;
  short int& player_turn = GameManager::instance().player_turn_counter_;

  float points[10];

  char heal_p[32], magic_p[4], attack_p[4], defense_p[4],
       dodge_p[4], level[4], movements[4];




  itoa(players[player_turn].heal_points_, heal_p, 10);
  itoa(players[player_turn].magic_points_, magic_p, 10);
  itoa(players[player_turn].attack_power_, attack_p, 10);
  itoa(players[player_turn].defense_power_, defense_p, 10);
  itoa(players[player_turn].dodge_power_, dodge_p, 10);
  itoa(players[player_turn].current_level_, level, 10);

  character_stats_name_.drawText(25, 100, players[player_turn].name_);
  character_stats_texts_[0].drawText(10, 140, "Heal: ");
  character_stats_texts_[1].drawText(10, 180, "Mana: ");
  character_stats_texts_[2].drawText(10, 220, "Attack: ");
  character_stats_texts_[3].drawText(10, 260, "Defense: ");
  character_stats_texts_[5].drawText(10, 300, "Level: ");




  character_stats_numbers_[0].drawText(95, 140, heal_p);
  character_stats_numbers_[1].drawText(95, 180, magic_p );
  character_stats_numbers_[2].drawText(95, 220, attack_p);
  character_stats_numbers_[3].drawText(95, 260, defense_p);
  character_stats_numbers_[5].drawText(95, 300, level);


}

void CombatScene::enemyStats() {

  GameManager& G = GameManager::instance();
  Character *players = GameManager::instance().my_characters_;
  Character *enemy = GameManager::instance().enemies_;
  Grid& grid = GameManager::instance().my_walking_grid_;
  short int& player_turn = GameManager::instance().player_turn_counter_;

  float points[10];

  char heal_p[4], magic_p[4], attack_p[4], defense_p[4],
       dodge_p[4], level[4], movements[4];



  itoa(enemy[G.enemy_id_].heal_points_, heal_p, 10);
  itoa(enemy[G.enemy_id_].magic_points_, magic_p, 10);
  itoa(enemy[G.enemy_id_].attack_power_, attack_p, 10);
  itoa(enemy[G.enemy_id_].defense_power_, defense_p, 10);
  itoa(enemy[G.enemy_id_].dodge_power_, dodge_p, 10);

  enemy_stats_texts_[0].drawText(700, 140, "Heal: ");
  enemy_stats_texts_[1].drawText(700, 180, "Mana: ");
  enemy_stats_texts_[2].drawText(700, 220, "Attack: ");
  enemy_stats_texts_[3].drawText(700, 260, "Defense: ");

  enemy_stats_numbers_[0].drawText(770, 140, heal_p);
  enemy_stats_numbers_[1].drawText(780, 180, magic_p );
  enemy_stats_numbers_[2].drawText(780, 220, attack_p);
  enemy_stats_numbers_[3].drawText(780, 260, defense_p);


}
void CombatScene::drawCombatTexts() {

  combat_texts_indicators_[0].drawText(20, 480, "Up-> Attack");
  combat_texts_indicators_[1].drawText(20, 510, "Down -> Magic");
  combat_texts_indicators_[2].drawText(20, 540, "Right ->Talk");
  combat_texts_indicators_[3].drawText(20, 570, "Left -> Fall Back");

  switch (drawable_text_indicator_) {

    case 1:
      combat_status_actions_texts_[0].drawText(220, 480, "Come here eared!");
      break;

    case 2:
      combat_status_actions_texts_[1].drawText(220, 480, "BiguiliBAMbiguiliBUM");
      break;

    case 3:
      combat_status_actions_texts_[2].drawText(220, 480, "Goodbye my lover, good bye my friend");

      break;

    case 4:
      combat_status_actions_texts_[3].drawText(220, 480, "Do u like pinya colada?");
      break;

    case 5:
      combat_status_actions_texts_[4].drawText(220, 480, "You dont know magic dude");
      break;

  }

  if (action_) {

    combat_status_actions_texts_[3].drawText(220, 540, "Press space to return to the game scene.");


  }
}


void CombatScene::attack() {

  GameManager& G = GameManager::instance();
  Character *players = GameManager::instance().my_characters_;
  Character *enemies = GameManager::instance().enemies_;
  Grid& grid = GameManager::instance().my_walking_grid_;
  short int& player_turn = GameManager::instance().player_turn_counter_;

  drawable_text_indicator_ = 1;

  if (enemies[G.enemy_id_].defense_power_ <= players[player_turn].attack_power_) {
    enemies[G.enemy_id_].heal_points_ = (enemies[G.enemy_id_].heal_points_ +
                                         enemies[G.enemy_id_].defense_power_)
                                        - players[player_turn].attack_power_;
    players[player_turn].experience_ += 100;
  }

  if (players[player_turn].enum_type_ != 5) {
    action_ = true;
  }
  else {
    G.sword_man_attack_ += 1;
    if (G.sword_man_attack_ > 1) {
      players[player_turn].heal_points_ -= players[player_turn].attack_power_;
      action_ = true;
      G.sword_man_attack_ = 0;
    }
  }

}

void CombatScene::attackWithMagic() {

  GameManager& G = GameManager::instance();
  Character *players = GameManager::instance().my_characters_;
  Character *enemies = GameManager::instance().enemies_;
  short int& player_turn = GameManager::instance().player_turn_counter_;

  if (players[player_turn].character_has_magic_) {
    drawable_text_indicator_ = 2;
    if (players[player_turn].magic_points_ >= 10) {

      players[player_turn].magic_points_ -= 10;
      enemies[G.enemy_id_].heal_points_ -= players[player_turn].attack_power_ * 2;

    }
    action_ = true;
  }
  else {

    drawable_text_indicator_ = 5;
  }




}

void CombatScene::runAway() {

  Character *players = GameManager::instance().my_characters_;
  Character *enemies = GameManager::instance().enemies_;
  short int& player_turn = GameManager::instance().player_turn_counter_;

  int run = rand() % 10;
  drawable_text_indicator_ = 3;
  if ((players[player_turn].dodge_power_ + run) > (run * 0.5)) {
    players[player_turn].experience_ += 1;
  }

  action_ = true;

}

void CombatScene::checkEnemyDead() {
  GameManager& G = GameManager::instance();
  Character *enemies = GameManager::instance().enemies_;
  Grid& grid = GameManager::instance().my_walking_grid_;

  if (enemies[G.enemy_id_].heal_points_ <= 0) {

    enemies[G.enemy_id_].alive_ = false;
    grid.data_[enemies[G.enemy_id_].array_position_] = 0;

  }
}
/*
void CombatScene::defeat() {
  Character *players = GameManager::instance().my_characters_;
  short int& player_turn = GameManager::instance().player_turn_counter_;

  if (players[player_turn].heal_points_ <= 0) {
    players[player_turn].alive_ = false;
  }

}
*/