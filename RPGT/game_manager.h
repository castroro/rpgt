/*
 *
 * Author: Alejandro Castro Royo <castroro@esat-alumni.com>
 *
 */


#ifndef __GAMEMAN_H__
#define __GAMEMAN_H__ 1

#include <stdlib.h>
#include <stdio.h>

#include <ESAT/window.h>
#include <ESAT/draw.h>
#include <ESAT/sprite.h>
#include <ESAT/input.h>
#include <time.h>
#include "soloud.h"
#include "soloud_wav.h"

//#include "game_manager.h"
#include "character.h"
#include "grid.h"
#include "mysprite.h"






class GameManager {

  public:

    static GameManager& instance() {
      static GameManager* my_instance;

      if (my_instance == NULL) {
        my_instance = new GameManager();
      }
      return *my_instance;
    };



    enum enum_of_Scene {
      kScene_IntroScene = 0,
      kScene_StartScene_Team,
      kScene_GameScene,
      kScene_CombatScene,
      kScene_ShopScene,
      kScene_GameOverScene,

    };
    /**
     * @brief objects of playable characters.
     *
     */

    Character my_characters_[4];
    /**
    * @brief objects of enemy characters.
    *
    */
    Character enemies_[20];

    /**
    * @brief array where the game is developed.
    *
    */
    Grid my_walking_grid_;

    /**
     * @brief enum of the possible scene.
     *
     */
    enum_of_Scene current_scene_;
    /**
    * @brief integger to know when the user is defeated.
    *
    */
    short int how_many_characters_are_alive_;
    /**
    * @brief know the turn of the player.
    *
    */
    short int player_turn_counter_;
    /**
     * @brief sound variables.
     *
     */
    SoLoud::Soloud soloud;
    SoLoud::Wav myWav;

    short int enemy_counter_;
    int enemy_id_;

    short int battle_indicator_;

    short int sword_man_attack_;




  private:

    GameManager();
};


#endif
