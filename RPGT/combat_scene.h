/*
 *
 * Author: Alejandro Castro Royo <castroro@esat-alumni.com>
 *
 */


#ifndef __COMBAT_SCENE_H__
#define __COMBAT_SCENE_H__ 1


#include "game_manager.h"
#include "input.h"
#include "mysprite.h"
#include "mytext.h"

class CombatScene {
  public:

    CombatScene();
    ~CombatScene();


    void init();
    void input();
    void update();
    void draw();
    /**
     * @brief      { dicrease the enemy health substrackting the character attack }
     */
    void attack();
    /**
     * @brief      { dicrease the enemy health substrackting the character magic attack }
     */
    void attackWithMagic();
    /**
     * @brief      { makes a rand to know if the character can scape of
     * the battle }
     */
    void runAway();
    /**
     * @brief      { draw the solid paths of the scene }
     */
    void infoSquares();
    void drawCombatTexts();
    /**
     * @brief      {show the character current stats in game}
     */
    void playerStats();
    void enemyStats();


    void checkEnemyDead();

    bool end_combat_;
    ///concretes the character had make a valid movement
    bool action_;
    short int drawable_text_indicator_;
    Input my_input_;

    Text end_combat_indicator_;

    Text combat_texts_indicators_[4];
    Text combat_status_actions_texts_[5];

    Text character_stats_texts_[6];
    Text character_stats_numbers_[6];
    Text enemy_stats_texts_[6];
    Text enemy_stats_numbers_[6];
    Text character_stats_name_;

    Sprite back_ground_image_;


  private:



};


#endif