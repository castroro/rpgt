var searchData=
[
  ['character_5fhas_5fmagic_5f',['character_has_magic_',['../class_character.html#a2d28144185325376dc4d506eac468b72',1,'Character']]],
  ['character_5fstats_5fname_5f',['character_stats_name_',['../class_combat_scene.html#acfa907a5b9fa0491d47dae0b7a73b843',1,'CombatScene']]],
  ['character_5fstats_5fnumbers_5f',['character_stats_numbers_',['../class_combat_scene.html#a0a04b3fe482e67e9afd072a7989620d2',1,'CombatScene::character_stats_numbers_()'],['../class_game_scene.html#a0ea925dd2da7f11153a23585406ef5af',1,'GameScene::character_stats_numbers_()']]],
  ['character_5fstats_5ftexts_5f',['character_stats_texts_',['../class_combat_scene.html#a4fababade1275fd8cb400897e7626cb9',1,'CombatScene::character_stats_texts_()'],['../class_game_scene.html#ad4136dbbd81067471e09cef0796f1bda',1,'GameScene::character_stats_texts_()']]],
  ['characters_5fnames_5f',['characters_names_',['../class_choose_team_scene.html#ad2cb41a4c4a6b394a9662d2de5ed19cf',1,'ChooseTeamScene']]],
  ['characters_5fsprites_5f',['characters_sprites_',['../class_choose_team_scene.html#a7b14df58dceea0423e10b1becd7c6e42',1,'ChooseTeamScene::characters_sprites_()'],['../class_game_scene.html#a2c31a493830ac9195cc468d1aa6cd620',1,'GameScene::characters_sprites_()']]],
  ['characters_5fzoom_5f',['characters_zoom_',['../class_game_scene.html#a6ce972a1e08cf9c6065422ad86ac37b0',1,'GameScene']]],
  ['choose_5fteam_5fscene_5f',['choose_team_scene_',['../class_scene_selector.html#a673176c332a1d3795f7bd94b5f6c8012',1,'SceneSelector']]],
  ['cols_5f',['cols_',['../class_grid.html#af63aa377f1e7de393f28dbc4ae310de5',1,'Grid']]],
  ['combat_5fscene_5f',['combat_scene_',['../class_scene_selector.html#ae127398434e35fc5e318f50d9eac0966',1,'SceneSelector']]],
  ['combat_5fstatus_5factions_5ftexts_5f',['combat_status_actions_texts_',['../class_combat_scene.html#a7e86f00a0c5c0f73a2da87bfb6264331',1,'CombatScene']]],
  ['combat_5ftexts_5findicators_5f',['combat_texts_indicators_',['../class_combat_scene.html#a90306c8c3c85eaf7e91d4d9e63b01dfc',1,'CombatScene']]],
  ['confirm_5faction_5f',['confirm_action_',['../class_input.html#a687202cc059ddfeae62104f63e2bf205',1,'Input']]],
  ['current_5flevel_5f',['current_level_',['../class_character.html#ac9939595a638c41f7c307a8c72076947',1,'Character']]],
  ['current_5fscene_5f',['current_scene_',['../class_game_manager.html#a35e6c0cee6c9051d04d3f18b98aca946',1,'GameManager']]]
];
